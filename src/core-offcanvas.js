/*
 *     Date: 2023
 *  Package: core-offcanvas
 *   Author: dbf, pacmanrulez, wize-wiz, wizdom
 *   Source: https://gitlab.com/pacmanrulez/core-offcanvas
 *  License: WTFPL v2 (http://www.wtfpl.net/txt/copying/)
 *
 *
 */

module.exports = (function () {

    if(!document) {
        throw new Error('Should run a in an environment where DOMDocument is available.');
    }

    const
        noop = () => {},
        PACKAGE = {         // package description
            name: 'Offcanvas'
        },
        COLLECTION = [],
        DOCUMENT = document,
        WINDOW = window,
        EVENT_TYPE = 'click',

        // dependencies
        EventBus = require('core-event-bus'),
        Scroller = require('core-element-scroller'),
        DOM = {
            ...require('core-utilities/utility/dom/get')
        },
        Package = require('core-utilities/utility/package'),

        // selector queries
        QUERY = {
            offcanvas: '[data-offcanvas-type]',
            offcanvas_name: '[data-offcanvas-type="?"]',
            offcanvas_close: '[data-offcanvas-close="?"]',
            offcanvas_close_inmenu: '[data-offcanvas-close]',
            offcanvas_trigger_name: '[data-offcanvas-trigger="?"]',
            offcanvas_scroller: '[data-offcanvas-scroller]',
            offcanvas_open_class: 'offcanvas-open-?'
        },

        // default options
        _default_options = {
            resizeDelay: 500,
            openClass: QUERY.offcanvas_open_class,
            transitionDelay: 300,
            trigger: null,

            menuElement: null,
            menuLink: 'a.menu-link',
            menuLinkActiveClass: 'li.active',
            /**
             * menuLinkCallback: <default> false
             * If menu element items should add a click event for
             */
            menuLinkCallback: noop,
            closeOnLinkClick: false,

            close: '[data-offcanvas="offcanvas-close"]',

            bodyLock: true,
            bodyLockClass: 'offcanvas-open',
            bodyLockTarget: 'body',

            scrollToActive: true,
            scrollToActiveDelay: 100,

            closeOnBodyClick: false,
            closeOnRetrigger: true,
        }
    ;

    const Offcanvas = function(name, options) {
        options = {
            ..._default_options,
            ...options
        };

        this.getName = function() {
            return name;
        }

        const
            BODY = DOM.get('body'),
            OFFCANVAS = DOM.get(QUERY.offcanvas_name.replace('?', name)),
            TRIGGERS = DOM.get(options.trigger ?
                options.trigger :
                QUERY.offcanvas_trigger_name.replace('?', name)),
            SCROLLER = DOM.get(options.scroller ?
                options.scroller :
                QUERY.offcanvas_scroller, OFFCANVAS),
            CLOSE = DOM.get(options.close ?
                options.close :
                QUERY.offcanvas_close.replace('?', name)),
            CLOSE_INMENU = DOM.get(QUERY.offcanvas_close_inmenu, OFFCANVAS),
            MENU = options.menuElement ? DOM.get(options.menuElement) : SCROLLER
        ;

        const
            EVENT_OPENED = 'opened',
            EVENT_CLOSED = 'closed'
        ;

        let _is_offcanvas_open = false,
            _body_lock_element,
            _resize_delay_timer;

        // validate all elements
        if(!(function() {
            // check all elements
            return true;
        })()) {
            throw new Error('Elements could not be validated');
        }

        let self = this;

        const _BODYClickCallback = function(event) {
            event.preventDefault();
            event.stopPropagation();
            event.stopImmediatePropagation();
            // close the menu.
            self.close();
        };

        let _init = function() {
            options.openClass = _getOpenClass();

            _is_offcanvas_open = self.isOpen();
            _setGeneralEvents();
            _setOpenEvents();
            _setCloseEvents();
            _initMenu();
            if(options.bodyLock) {
                _prepareBodyLock();
            }
            if(_is_offcanvas_open) {
                _scrollToActive();
            }
        }

        let _getOpenClass = function() {
            return options.openClass.replace('?', name);
        };

        let _setGeneralEvents = function() {
            _setResizeEvent();
            _setClickOffcanvasEvent();
            _setListeners();
        };

        let _setListeners = function() {
            OFFCANVAS.addEventListener(EVENT_OPENED, () => {
                console.log(EVENT_OPENED + ' called');
                if(options.scrollToActive) {
                    console.log('scrolling to active');
                    _scrollToActive();
                }
            });
        };

        let _setClickOffcanvasEvent = function() {
            OFFCANVAS.addEventListener(EVENT_TYPE, (event) => {
                event.preventDefault();
                event.stopPropagation();
                event.stopImmediatePropagation();
            })
        };

        let _setResizeEvent = function() {
            WINDOW.addEventListener('resize', () => {
                if(_resize_delay_timer) {
                    clearTimeout(_resize_delay_timer);
                }
                _resize_delay_timer = setTimeout(function() {
                    // _scrollToActive() ?
                }, options.resizeDelay);
            });
        };

        let _setOpenEvents = function() {
            TRIGGERS.addEventListener(EVENT_TYPE, function(event) {
                event.preventDefault();
                event.stopPropagation();
                event.stopImmediatePropagation();

                if(self.isOpen() && options.closeOnRetrigger === false) {
                    return;
                }

                self[self.isOpen() ? 'close' : 'open']();
            });
        };

        let _setCloseEvents = function() {
            let callback = (event) => {
                event.preventDefault();
                event.stopPropagation();
                event.stopImmediatePropagation();

                self.close();
            };
            // default in-menu close button
            CLOSE_INMENU.addEventListener(EVENT_TYPE, callback);
            // additional close buttons
            if(CLOSE) {
                CLOSE.addEventListener(EVENT_TYPE, callback);
            }
        };

        let _setBodyClickEvent = function() {
            DOCUMENT.querySelector('body').addEventListener(EVENT_TYPE, _BODYClickCallback);
        };

        let _unsetBodyClickEvent = function() {
            DOCUMENT.querySelector('body').removeEventListener(EVENT_TYPE, _BODYClickCallback);
        };

        let _initMenu = function(element_callback) {
            DOCUMENT.querySelectorAll(options.menuLink).forEach((link) => {
                link.addEventListener(EVENT_TYPE, (event) => {
                    event.stopImmediatePropagation();
                    options.menuLinkCallback();
                    if(options.closeOnLinkClick) {
                        self.close();
                    }
                });
            });
        };

        let _findActiveElement = function() {
            return MENU.querySelector(options.menuLinkActiveClass);
        };

        let _scrollToActive = function() {
            setTimeout(() => {
                let active = _findActiveElement();
                if(active) {
                    Scroller.scrollTo([active, SCROLLER]);
                }
            }, options.scrollToActiveDelay);
        };

        let _prepareBodyLock = function() {
            _body_lock_element = DOCUMENT.querySelector(options.bodyLockTarget);
            _unsetBodyLock();
        };

        let _setBodyLock = function() {
            _body_lock_element.classList.add(options.bodyLockClass);
        };

        let _unsetBodyLock = function() {
            let removeBODY_lock = true;

            if(COLLECTION.length) {
                for (let index in COLLECTION) {
                    let offcanvas = COLLECTION[index]
                    if(removeBODY_lock === false) { break; }
                    if(offcanvas === self) { continue; }
                    if(offcanvas.isOpen() && offcanvas.hasBodyLock()) { removeBODY_lock = false; }
                }
            }

            if(removeBODY_lock) {
                _body_lock_element.classList.remove(options.bodyLockClass);
            }
        };

        this.hasBodyLock = function() {
            return options.bodyLock;
        }

        this.isOpen = function() {
            return BODY.classList.contains(options.openClass);
        }

        this.open = function() {
            if(this.isOpen()) {
                return;
            }
            _is_offcanvas_open = true;
            BODY.classList.add(options.openClass)
            if(options.bodyLock) {
                _setBodyLock();
            }
            if(options.closeOnBodyClick) {
                _setBodyClickEvent();
            }
            setTimeout(() => {
                // if offcanvas is still open
                if(this.isOpen()) {
                    OFFCANVAS.dispatchEvent(new Event('opened'));
                }
            }, options.transitionDelay);
        }

        this.close = function() {
            if(!this.isOpen()) {
                return;
            }
            _is_offcanvas_open = false;
            BODY.classList.remove(options.openClass)
            if(options.bodyLock) {
                _unsetBodyLock();
            }
            if(options.closeOnBodyClick) {
                _unsetBodyClickEvent();
            }
        }

        this.element = function(name) {
            switch(name) {
                case 'offcanvas': return OFFCANVAS; break;
                case 'scroller': return SCROLLER; break;
                case 'menu': return MENU; break;
            }
            return false;
        }

        _init();
    };

    Offcanvas.inst = function(name, options) {
        let offcanvas = new PACKAGE.Offcanvas(name, options);
        COLLECTION.push(offcanvas);
        return offcanvas;
    };

    Offcanvas.all = function() {
        return COLLECTION;
    };

    Offcanvas.get = function(name) {
        for(let index in COLLECTION) {
            let offcanvas = COLLECTION[index];
            if(offcanvas.getName() === name) {
                return offcanvas;
            }
        }
        return false;
    };

    Offcanvas.closeAll = function() {
        for(let index in COLLECTION) {
            COLLECTION[index].close();
        }
    }

    Offcanvas.openAll = function() {
        for(let index in COLLECTION) {
            COLLECTION[index].open();
        }
    }

    PACKAGE.Offcanvas = Offcanvas;
    // assign to global object core
    return Package.return(PACKAGE);

})();