/*
 *     Date: 2023
 *  Package: core-offcanvas
 *   Author: dbf, pacmanrulez, wize-wiz, wizdom
 *   Source: https://gitlab.com/pacmanrulez/core-offcanvas
 *  License: WTFPL v2 (http://www.wtfpl.net/txt/copying/)
 *
 *
 */

const path = require('path');

module.exports = {
  mode: 'production',
  entry: './src/core-offcanvas.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'core-offcanvas.js',
    library: "coreOffcanvas",
    libraryTarget: 'umd',
    globalObject: 'this'
  }
};
